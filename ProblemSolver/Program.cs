﻿// -----------------------------------------------------------------------
// <copyright file="Program.cs" company="Carazy.com">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace ProblemSolver
{
    using System.Collections.Generic;

    using ProjectEuler;
    using ProjectEuler.Problems;

    /// <summary>
    /// The program yo.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Mains the specified args.
        /// </summary>
        /// <param name="args">The args.</param>
        private static void Main(string[] args)
        {
            var problemsSolved = new List<IProblem>
                {
                    new Problem1(),
                    new Problem2(),
                    new Problem6()
                };

            foreach (var problem in problemsSolved)
            {
                Console.WriteLine(problem.GetType().Name + ": " + problem.ProblemDescription);
                Console.WriteLine("Solution: " + problem.GetAnswer());
                Console.WriteLine();
            }

            Console.Read();
        }
    }
}