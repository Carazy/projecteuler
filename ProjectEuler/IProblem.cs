﻿// -----------------------------------------------------------------------
// <copyright file="IProblem.cs" company="Carazy.Com">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace ProjectEuler
{
    /// <summary>
    /// The interface for problems
    /// </summary>
    /// <typeparam name="T">The type of answer</typeparam>
    public interface IProblem
    {
        /// <summary>
        /// Gets the description of the problem.  The majority of the time this
        /// will come directly from the ProjectEuler site.
        /// </summary>
        string ProblemDescription { get; }

        /// <summary>
        /// Solves the problem.
        /// </summary>
        /// <remarks>How did you come to this conclusion?</remarks>
        /// <returns>The answer to the problem</returns>
        int GetAnswer();
    }
}