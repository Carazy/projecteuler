﻿// -----------------------------------------------------------------------
// <copyright file="Problem1.cs" company="Carazy.com">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace ProjectEuler.Problems
{
    /// <summary>
    /// If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
    /// PROBLEM: Find the sum of all the multiples of 3 or 5 below 1000.
    /// </summary>
    public class Problem1 : IProblem
    {
        /// <summary>
        /// Gets the description of the problem.  The majority of the time this
        /// will come directly from the ProjectEuler site.
        /// </summary>
        public string ProblemDescription
        {
            get
            {
                return "Find the sum of all the multiples of 3 or 5 below 1000";
            }
        }

        /// <summary>
        /// Solves the problem.
        /// </summary>
        /// <remarks>
        /// My solution to this problem is going to be to loop through all
        /// numbers to 1000 and for each one check if they're multiples of 3 or
        /// 5 and sum them if they are.
        /// </remarks>
        /// <returns>The answer to the problem</returns>
        public int GetAnswer()
        {
            var total = 0;

            for (var n = 0; n < 1000; n++)
            {
                if (n % 3 == 0 || n % 5 == 0)
                {
                    total += n;
                }
            }

            return total;
        }
    }
}