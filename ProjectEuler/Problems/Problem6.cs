﻿// -----------------------------------------------------------------------
// <copyright file="Problem3.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace ProjectEuler.Problems
{
    /// <summary>
    /// <para>The sum of the squares of the first ten natural numbers is,</para>
    /// <code>12 + 22 + ... + 102 = 385</code>
    /// <para>The square of the sum of the first ten natural numbers is, </para>
    /// <code>(1 + 2 + ... + 10)2 = 552 = 3025</code>
    /// <para>Hence the difference between the sum of the squares of the first
    /// ten natural numbers and the square of the sum is 3025  385 = 2640.</para>
    /// <para>Find the difference between the sum of the squares of the first
    /// one hundred natural numbers and the square of the sum. </para>
    /// </summary>
    public class Problem6 : IProblem
    {
        /// <summary>
        /// Gets the description of the problem.  The majority of the time this
        /// will come directly from the ProjectEuler site.
        /// </summary>
        public string ProblemDescription
        {
            get
            {
                return
                    "Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.";
            }
        }

        /// <summary>
        /// Solves the problem.
        /// </summary>
        /// <remarks>How did you come to this conclusion?</remarks>
        /// <returns>The answer to the problem</returns>
        public int GetAnswer()
        {
            var sumOfSquares = this.GetSumOfSquares(100);
            var squareOfSums = this.GetSquareOfSums(100);

            var difference = squareOfSums - sumOfSquares;

            return difference;
        }

        /// <summary>
        /// Gets the square of sums.
        /// </summary>
        /// <param name="limit">The limit.</param>
        /// <returns>the square of the sums</returns>
        private int GetSquareOfSums(int limit)
        {
            var squareOfSums = 0;

            for (var n = 1; n <= limit; n++)
            {
                squareOfSums += n;
            }

            return squareOfSums * squareOfSums;
        }

        /// <summary>
        /// Gets the sum of squares.
        /// </summary>
        /// <param name="limit">The limit.</param>
        /// <returns>The sums of the square</returns>
        private int GetSumOfSquares(int limit)
        {
            var sumOfSquares = 0;

            for (var n = 1; n <= limit; n++)
            {
                sumOfSquares += n * n;
            }

            return sumOfSquares;
        }
    }
}